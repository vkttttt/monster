(function ($) {



    $(document).ready(function () {

        $(".tResinline").each(function () {
            $w = $(this).children('img').attr('width');
            $(this).css('width', $w);
        });

        $("img.imgres").each(function () {
            w = $(this).attr('width');
            h = $(this).attr('height');
            $(this).wrap('<span class="spanres" style="width:' + w + 'px;display: inline-block; position: relative;"></span>');
            $(this).removeClass('imgres').css({ 'position': 'absolute', 'object-fit': 'contain', 'top': '0', 'left': '0', 'width': '100%', 'height': '100%' });
            pr = $(this).parent();
            sp = $('<span style="padding-top:' + (h / w * 100) + '%;display: block; position:relative;">');
            if ($(this).hasClass('img')) {
                $(this).removeClass('img');
                pr.addClass('img');
            }
            pr.prepend(sp);
        });

        // check next
        $(document).on('click', '#Next', function(e){
            var error = false;
            // check
            var field33 = $('input[name="bill_number"]');
            var field34 = $('input[name="mg_34_field"]');
            var field33_val = field33.val();
            var field34_val = field34.val();
            if(field33_val == ''){
                if(field33.parent().find('span.error').length>0){
                    field33.parent().find('span.error').show();
                }else{
                    field33.parent().append('<span class="error">Số hoá đơn mua hàng không được để trống.</span>');
                }
                
                error = true;
            }else if(field33_val.length<5){
                if(field33.parent().find('span.error').length>0){
                    field33.parent().find('span.error').show();
                }else{
                    field33.parent().append('<span class="error">Số hoá đơn phải từ 5 ký tự trở lên.</span>');
                }
                
                error = true;
            }

            if(document.getElementById("bill-img").files.length == 0){
                if(field34.parent().find('.error').length>0){
                    field34.parent().find('.error').show();
                }else{
    
                    field34.parent().append('<span class="error">Ảnh hóa đơn là bắt buộc.</span>');
                }
                
                error = true;
            }

            if(error == true){
                return false;
            }

            e.preventDefault();
            // success
            $('.step1').removeClass('show');
            $('.mt-1').removeClass('show');
            $('.step2').addClass('show');
            $('.mt-2').addClass('show');
        });
        $(':input').on('propertychange input', function (e) {
            if($(this).val() != ''){
                if($(this).parent().find('.error').length>0){
                    $(this).parent().find('.error').hide();
                }
            }
        });

        // previous
        $('#Previous').on('click',function(e){
            e.preventDefault();
            // success
            $('.step2').removeClass('show');
            $('.mt-2').removeClass('show');
            $('.step1').addClass('show');
            $('.mt-1').addClass('show');
        });

        // function setInputFilter(textbox, inputFilter) {
        //     ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
        //         textbox.addEventListener(event, function () {
        //             if (inputFilter(this.value)) {
        //                 this.oldValue = this.value;
        //                 this.oldSelectionStart = this.selectionStart;
        //                 this.oldSelectionEnd = this.selectionEnd;
        //             } else if (this.hasOwnProperty("oldValue")) {
        //                 this.value = this.oldValue;
        //                 this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        //             }
        //         });
        //     });
        // }

        // // Restrict input to digits and '.' by using a regular expression filter.
        // setInputFilter(document.getElementById("bill-number"), function (value) {
        //     return /^\d*\.?\d*$/.test(value);
        // });
        // setInputFilter(document.getElementById("name"), function (value) {
        //     return /^[a-z\u00c0-\u024f]*$/i.test(value);
        // });
        // setInputFilter(document.getElementById("number-cmnd"), function (value) {
        //     return /^\d*\.?\d*$/.test(value);
        // });
        // setInputFilter(document.getElementById("phone-number"), function (value) {
        //     return /^\d*\.?\d*$/.test(value);
        // });
        /* click show menu */
        const $menu = $('.menu');

        $(document).mouseup(function (e) {
            if (!$menu.is(e.target) // if the target of the click isn't the container...
                &&
                $menu.has(e.target).length === 0) // ... nor a descendant of the container
            {
                $('body').removeClass('menu-slows');
            } else {

            }
        });


        $('.nav-mobile,.menu-close').click(function () {
            $('body').toggleClass('menu-slows');
        });

        // $('.mainmenu li.parent > a').click(function(e) {
        //     e.preventDefault();
        // })

        $('.mainmenu li a').click(function (e) {
            $('.mainmenu li a').removeClass('active');
            $(this).addClass('active');
        });

        // mobile submenu
        $('.mainmenu li.parent').each(function () {
            var p = $(this),
                btn = $('<span>', {
                    'class': 'showsubmenu fa fa-caret-right',
                    text: ''
                }).click(function () {
                    if (p.hasClass('parent-showsub')) {
                        menu.slideUp(300, function () {
                            p.removeClass('parent-showsub');
                        });
                    } else {
                        menu.slideDown(300, function () {
                            p.addClass('parent-showsub');
                        });
                    };
                }),
                menu = p.children('ul');
            p.prepend(btn);
        });





        function goToLink(link, marginTop) {
            $('.mainmenu li a[href="' + link + '"]').click(function () {
                //$('body').removeClass('showMenu');
                $('body').toggleClass('menu-slows');
                $('html, body').animate({
                    scrollTop: $(link).offset().top - marginTop
                }, 1000);
            });
        }

        if ($(window).width() > 480) {
            $(".mainmenu li a[href*='#']").each(function () {
                var link = $(this).attr('href');
                goToLink(link, 88);
            });
        } else {
            $(".mainmenu li a[href*='#']").each(function () {
                var link = $(this).attr('href');
                goToLink(link, 88);
            });
        }

        // LAZYLOAD
        /*-----------------------------------------------------------------*/
        "use strict";
        var BJLL_options = BJLL_options || {},
            BJLL = {
                _ticking: !1,
                check: function () {
                    if (!BJLL._ticking) {
                        BJLL._ticking = !0, void 0 === BJLL.threshold && (void 0 !== BJLL_options.threshold ? BJLL.threshold = parseInt(BJLL_options.threshold) : BJLL.threshold = 200);
                        var e = document.documentElement.clientHeight || body.clientHeight,
                            t = !1,
                            n = document.getElementsByClassName("lazy-hidden");
                        [].forEach.call(n, function (n, a, i) {
                            var s = n.getBoundingClientRect();
                            e - s.top + BJLL.threshold > 0 && (BJLL.show(n), t = !0)
                        }), BJLL._ticking = !1, t && BJLL.check()
                    }
                },      
                show: function (e) {
                    e.className = e.className.replace(/(?:^|\s)lazy-hidden(?!\S)/g, ""), e.addEventListener("load", function () {
                        e.className += " lazy-loaded", BJLL.customEvent(e, "lazyloaded");
                    }, !1);
                    var t = e.getAttribute("data-lazy-type");
                    if ("image" == t) null != e.getAttribute("data-lazy-srcset") && e.setAttribute("srcset", e.getAttribute("data-lazy-srcset")), null != e.getAttribute("data-lazy-sizes") && e.setAttribute("sizes", e.getAttribute("data-lazy-sizes")), e.setAttribute("src", e.getAttribute("data-lazy-src"));

                    else if ("bg" == t) {
                        var n = e.getAttribute("data-lazy-src");
                        e.style.backgroundImage = 'url(' + n + ')';
                        e.className += ' lazy-loaded';
                    } else if ("iframe" == t) {
                        var n = e.getAttribute("data-lazy-src"),
                            a = document.createElement("div");
                        a.innerHTML = n;
                        var i = a.firstChild;
                        e.parentNode.replaceChild(i, e);
                    }
                },
                customEvent: function (e, t) {
                    var n;
                    document.createEvent ? (n = document.createEvent("HTMLEvents")).initEvent(t, !0, !0) : (n = document.createEventObject()).eventType = t, n.eventName = t, document.createEvent ? e.dispatchEvent(n) : e.fireEvent("on" + n.eventType, n)
                }
            };
        window.addEventListener("load", BJLL.check, !1), window.addEventListener("scroll", BJLL.check, !1), window.addEventListener("resize", BJLL.check, !1), document.getElementsByTagName("body").item(0).addEventListener("post-load", BJLL.check, !1);

        //Menu Store
        $('.submenu.store li a').click(function () {
            var tabs = $(this).attr('data-tabs');
            $('.bill-content').hide();
            var activeTab = tabs;
            $(activeTab).fadeIn();

            $('#store-tabs li a').each(function () {
                var at = $(this).attr('href');
                if (at == activeTab) {
                    $('#store-tabs li a').removeClass('active');
                    $(this).addClass('active');
                }
            });

            return false;

        });
        //Store & Bill


        $('#store-tabs option:first').addClass('active');
        $('#store-tabs').val();
        $('.bill-content').hide();
        $('.bill-content:first').show();
        $('#store-tabs').change(function () {
            $("#store-tabs option:selected").each(function () {
                $('#store-tabs option').removeClass('active');
                $(this).addClass('active');
                $('.bill-content').hide();
                var activeTab = $(this).val();
                $("#" + activeTab).fadeIn();
                return false;
            });
        });
        //popup

        //FAQ
        var acc = document.getElementsByClassName("question");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }

        //Zoom Image

        // $('.zoom').click(function() {
        //     var src = $(this).attr('data-value');
        //     $('#myModal').show();
        //     $('img.modal-content').attr('src', 'img/'+src);
        //     console.log($('img.modal-content').attr('src','img/'+src));
        // });

        $(function () {
            $('div[id^="video"]').on('hidden.bs.modal', function (e) {
                var src = $(this).find("img");
                src.attr("src", src.attr("src"));
            });

            $(window).resize();

        });
        var getVideoUrl = function (src) {
            return 'assets/img/bill/' + src;
        }
        $(".zoom-img").each(function () {
            $(this).click(function () {
                var src = $(this).attr('data-value');
                $('.video_iframe img').attr('src', getVideoUrl(src));
            });
        });
        //Upload File

    });
})(jQuery);

(function (document, window, index) {
    var inputs = document.querySelectorAll('.file-upload');
    Array.prototype.forEach.call(inputs, function (input) {
        var label = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener('change', function (e) {
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else
                fileName = e.target.value.split('\\').pop();

            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });

        // Firefox bug fix
        input.addEventListener('focus', function () { input.classList.add('has-focus'); });
        input.addEventListener('blur', function () { input.classList.remove('has-focus'); });
    });
}(document, window, 0));

