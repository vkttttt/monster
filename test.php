<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Monster Energy</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://allfont.net/allfont.css?fonts=roboto-bold" rel="stylesheet" type="text/css">
        <link href="assets/img/favicon.png" rel="shortcut icon" type="image/vnd.microsoft.icon">
        <link rel="stylesheet" href="https://gear.monsterenergyvietnam.com/assets/css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="assets/css/font-awesome.min.css">
        <!-- font-cdn -->
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
        <link type="text/css" rel="stylesheet" href="assets/css/style.css">
        <link type="text/css" rel="stylesheet" href="assets/css/main.css">
        <link type="text/css" rel="stylesheet" href="assets/fonts/fonts.css">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160974947-1"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-84284805-2"></script>
        <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-84284805-2');
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160974947-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-160974947-1');
        </script>
    </head>
    <body>
        <header id="home">
            <div class="container">
                <div class="row">
                    <div class="col-3 logo">
                        <a href="./">
                            <img class="lazy-hidden" src="assets/img/logo-monster.png">
                        </a>
                    </div>
                    <div class=" col-9 home-menu">
                        <div class="menu mainmenu vi">
                            <span class="menu-close">
                                <a href="javascript:void(0)">
                                    <i class="fa fa-times"></i>
                                </a>
                            </span>
                            <ul class="list-unstyled">
                                <li>
                                    <a href="#join">
                                        Đăng ký tham gia
                                        <i class="far fa-chevron-down"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="./rules">
                                        Thể lệ
                                        <i class="far fa-chevron-down"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#reward">
                                        Cách tham gia
                                        <i class="far fa-chevron-down"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#faq">
                                        Giải thưởng
                                        <i class="far fa-chevron-down"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="./list-prize" class="label-name">
                                        Danh sách trúng thưởng
                                        <i class="far fa-chevron-down"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="nav-mobile">
                            <i class="fa fa-bars"></i>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section id="banner">
            <div class="banner hidden-mb">
                <img class="lazy-hidden" src="assets/img/Monster Yamaha Exciter Bike Promo - Vietnam KV Adaptation - Horizontal - 1800 838 PNG.png" alt="Banner">
            </div>
            <div class="banner hidden-pc">
                <img class="lazy-hidden" src="assets/img/Monster Yamaha Exciter Bike Promo - Vietnam KV Adaptation - Horizontal - 1 1 RGB.jpg" alt="Banner">
            </div>
            <div class="arrow-flicker btn-scroll-down-fixed">
                <img class="lazy-hidden" src="assets/img/mouse.png" alt="mouse">
            </div>
        </section>
        <section id="join-now">
            <div class="container mb-5">
                <div class="wrapper-title-skew">
                    <div class="title-main mb-30 skew-xy">
                        <div class="title-join-now-text">
                            <h2>Tham gia ngay</h2>
                        </div>
                    </div>
                </div>
                <div class="chevron-down-join-now">
                    <i class="far fa-chevron-down"></i>
                </div>
                <div class="form-register-join">
                    <div class="title-main mb-30">
                        <h2>
                            <span>đăng ký tham gia</span>
                        </h2>
                    </div>
                    <form class="form-register-content" id="form">
                        <div class="form-bill-area-wrap">
                            <div class="form-bill-area">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">
                                        <label class="label-name">Chọn cửa hàng đã mua sản phẩm</label>
                                        <div class="register-box ">
                                            <select id="store-choose" class="box-content">
                                                <option value="#" selected>Circle K</option>
                                                <option value="#">Vinmart/Vinmart+</option>
                                                <option value="#">Lotte Mart</option>
                                                <option value="#">Bách Hóa Xanh</option>
                                                <option value="#">Co.opmart</option>
                                                <option value="#">Big C</option>
                                                <option value="#">Aeon</option>
                                                <option value="#">Ministop</option>
                                            </select>
                                            <i class="fas fa-caret-down"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <label class="label-name">Số hóa đơn mua hàng</label>
                                        <div class="register-box bill respon">
                                            <input
                                                type="text"
                                                class="box-content"
                                                id="bill-number"
                                                placeholder="Nhập số Hóa đơn mua hàng"
                                                required
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 upload">
                                        <div class="bill-upload">
                                            <input
                                                class="file-upload bill"
                                                type="file"
                                                id="bill-img"
                                                name="billimg"
                                                multiple
                                            >
                                            <label for="bill-img">
                                                <i class="fas fa-upload"></i>
                                                <span>Tải ảnh hóa đơn</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-user-area-wrap mt-2">
                            <div class="form-bill-area">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="label-name">Họ và tên</label>
                                        <div class="register-box bill">
                                            <input
                                                type="text"
                                                id="name"
                                                class="box-content-name"
                                                placeholder="Họ và tên"
                                                required
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="label-name">Ngày sinh</label>
                                        <div class="register-box bill">
                                            <input
                                                type="date"
                                                class="box-content"
                                                id="birthday"
                                                name="birthday"
                                                value="1998-07-10"
                                                min="1960-01-01"
                                                max="2001-12-31"
                                                required
                                            >
                                        </div>
                                        <i class="fal fa-calendar"></i>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="label-name">Số CMND</label>
                                        <div class="register-box bill">
                                            <input
                                                type="text"
                                                id="number-cmnd"
                                                class="box-content"
                                                placeholder="Nhập số CMND của bạn"
                                                maxlength="12"
                                                required
                                            >
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-md-4">
                                        <label class="label-name">Email</label>
                                        <div class="register-box bill">
                                            <input
                                                type="email"
                                                class="box-content"
                                                id="email"
                                                placeholder="Nhập email của bạn"
                                                required
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="label-name">Số điện thoại</label>
                                        <div class="register-box bill">
                                            <input
                                                type="text"
                                                id="phone-number"
                                                class="box-content"
                                                placeholder="Số điện thoại"
                                                maxlength="10"
                                                required
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="label-name">Địa chỉ nhận quà</label>
                                        <div class="register-box bill">
                                            <!-- <input type="text" placeholder="Nhập địa chỉ"> -->
                                            <textarea class=" box-content" placeholder="Nhập địa chỉ" required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="register-button mt-5">
                            <button type="submit" class="btn-register btn btn-primary">Đăng Ký</button>
                        </div>
                    </form>
                   <!-- Easy Forms -->
                    <div id="c31">
                        Fill out my <a href="https://form.mangoads.com.vn/app/form?id=31">online form</a>.
                    </div>
                   
                </div>
            </div>
        </section>
        <section id="join" class="padding">
            <div class="container">
                <div class="title-main mb-30">
                    <h2>
                        <span>Cách tham gia</span>
                    </h2>
                </div>
                <!-- <div class="desc">Hướng dẫn tham gia chương trình khuyễn mãi</div> -->
                <div class="box-step">
                    <div class="row justify-content-center">
                        <div class="col-lg-4 col-sm-8 step">
                            <div class="box-img">
                                <div class="img">
                                    <img class="lazy-hidden" src="assets/img/Unititled-removebg-preview.png" alt="">
                                </div>
                                <div class="bg-white-opacity"></div>
                            </div>
                            <div class="content">
                                <table>
                                    <tr>
                                        <th>
                                            Bước 1:
                                            <span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mua
                                            <strong>Monster Energy</strong>
                                            tại các cửa hàng có chương trình khuyến mãi
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-8 step">
                            <div class="box-img">
                                <div class="img">
                                    <img class="lazy-hidden" src="assets/img/Group 21.png" alt="">
                                </div>
                                <div class="bg-white-opacity"></div>
                            </div>
                            <div class="content">
                                <table>
                                    <tr>
                                        <th>
                                            Bước 2:
                                            <span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            đăng nhập website:
                                            <strong>
                                                <span class="link">
                                                    trungquamonster.com
                                                    <br>
                                                </span>
                                            </strong>
                                            để
                                            <strong>
                                                gửi thông tin và hình ảnh hóa đơn mua hàng
                                                <br>
                                                nhận mã số quay thưởng bằng tin nhắn điện thoại hoặc email
                                            </strong>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-8 step">
                            <div class="box-img">
                                <div class="img">
                                    <img class="lazy-hidden" src="assets/img/Group 42.png" alt="">
                                </div>
                                <div class="bg-white-opacity"></div>
                            </div>
                            <div class="content">
                                <table>
                                    <tr>
                                        <th>
                                            Bước 3:
                                            <span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            quay số trúng thưởng
                                            <span class="link">mỗi tuần</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="store" class="box-store">
                    <div class="row store-bill">
                        <div class="col-md-4 store">
                            <div class="title">Vui lòng chọn hệ thống mua hàng để tìm vị trí số hóa đơn tương ứng</div>
                            <select class="form-control" name="chosen" id="store-tabs">
                                <option value="CircleK">
                                    Circle K
                                </option>
                                <option value="Vinmart">
                                    Vinmart / Vinmart+
                                </option>
                                <option value="familyMart">
                                    Family Mart
                                </option>
                                <option value="BachHoaXanh">
                                    Bách Hóa Xanh
                                </option>
                                <option value="coopmart">
                                    Co.opmart
                                </option>
                                <option value="bigc">
                                    Big C
                                </option>
                                <option value="aeon">
                                    Aeon
                                </option>
                                <option value="ministop">
                                    Ministop
                                </option>
                            </select>
                            <div class="date">
                                <p>
                                    Thời gian đổi quà đến hết ngày
                                    <br>
                                    <span class="link">00.00.2020</span>
                                </p>
                                <p>
                                    Thời gian trao thưởng đến hết ngày
                                    <br>
                                    <span class="link">00.00.2020</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4 bill">
                            <div id="bill-content">
                                <div id="CircleK" class="bill-content">
                                    <div class="img">
                                        <img class="lazy-hidden" src="assets/img/Mau-hoa-don.jpg" alt="Circlek">
                                        <a
                                            href="javascript:void(0)"
                                            class="zoom-img"
                                            data-value="circlek.jpg"
                                            data-toggle="modal"
                                            data-target="#video"
                                        >
                                            <img class="zoom" src="assets/img/zoom.png" alt="Zoom">
                                        </a>
                                    </div>
                                </div>
                                <div id="Vinmart" class="bill-content">
                                    <div class="img">
                                        <img class="lazy-hidden" src="assets/img/bill/vinsmart.jpg" alt="Vinsmart">
                                        <a
                                            href="javascript:void(0)"
                                            class="zoom-img"
                                            data-value="vinsmart.jpg"
                                            data-toggle="modal"
                                            data-target="#video"
                                        >
                                            <img class="zoom" src="assets/img/zoom.png" alt="Zoom">
                                        </a>
                                    </div>
                                </div>
                                <div id="LotteMart" class="bill-content">
                                    <div class="img">
                                        <img class="lazy-hidden" src="assets/img/bill/lotte-mart.jpg" alt="Lotte Mart">
                                        <a
                                            href="javascript:void(0)"
                                            class="zoom-img"
                                            data-value="lotte-mart.jpg"
                                            data-toggle="modal"
                                            data-target="#video"
                                        >
                                            <img class="zoom" src="assets/img/zoom.png" alt="Zoom">
                                        </a>
                                    </div>
                                </div>
                                <div id="BachHoaXanh" class="bill-content">
                                    <div class="img">
                                        <img class="lazy-hidden" src="assets/img/bill/bhx.jpg" alt="Bách hóa xanh">
                                        <a
                                            href="javascript:void(0)"
                                            class="zoom-img"
                                            data-value="bhx.jpg"
                                            data-toggle="modal"
                                            data-target="#video"
                                        >
                                            <img class="zoom" src="assets/img/zoom.png" alt="Zoom">
                                        </a>
                                    </div>
                                </div>
                                <div id="coopmart" class="bill-content">
                                    <div class="img">
                                        <img class="lazy-hidden" src="assets/img/bill/bhx.jpg" alt="Bách hóa xanh">
                                        <a
                                            href="javascript:void(0)"
                                            class="zoom-img"
                                            data-value="bhx.jpg"
                                            data-toggle="modal"
                                            data-target="#video"
                                        >
                                            <img class="zoom" src="assets/img/zoom.png" alt="Zoom">
                                        </a>
                                    </div>
                                </div>
                                <div id="bigc" class="bill-content">
                                    <div class="img">
                                        <img class="lazy-hidden" src="assets/img/bill/bhx.jpg" alt="Bách hóa xanh">
                                        <a
                                            href="javascript:void(0)"
                                            class="zoom-img"
                                            data-value="bhx.jpg"
                                            data-toggle="modal"
                                            data-target="#video"
                                        >
                                            <img class="zoom" src="assets/img/zoom.png" alt="Zoom">
                                        </a>
                                    </div>
                                </div>
                                <div id="aeon" class="bill-content">
                                    <div class="img">
                                        <img class="lazy-hidden" src="assets/img/bill/bhx.jpg" alt="Bách hóa xanh">
                                        <a
                                            href="javascript:void(0)"
                                            class="zoom-img"
                                            data-value="bhx.jpg"
                                            data-toggle="modal"
                                            data-target="#video"
                                        >
                                            <img class="zoom" src="assets/img/zoom.png" alt="Zoom">
                                        </a>
                                    </div>
                                </div>
                                <div id="ministop" class="bill-content">
                                    <div class="img">
                                        <img class="lazy-hidden" src="assets/img/bill/bhx.jpg" alt="Bách hóa xanh">
                                        <a
                                            href="javascript:void(0)"
                                            class="zoom-img"
                                            data-value="bhx.jpg"
                                            data-toggle="modal"
                                            data-target="#video"
                                        >
                                            <img class="zoom" src="assets/img/zoom.png" alt="Zoom">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--End john-->
            <!--Start reward-->
            <section id="reward" class="padding">
                <div class="container">
                    <div class="title-main mb-30">
                        <h2>
                            <span>Giải Thưởng</span>
                        </h2>
                    </div>
                </div>
                <div class="reward-special">
                    <div class="reward-content">
                        <div class="reward-special-number">
                            <div class="reward-number">
                                <p>08</p>
                                <div class="reward-content-title">
                                    <div class="reward-title">
                                        <span>giải đặc biệt</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="reward-content-description">
                            Xe Exiter 150 phiên bản
                            <br>
                            <span>monster energy yamaha motogp</span>
                        </div>
                        <div class="reward-content-img">
                            <img src="assets/img/exMS-removebg-preview.png" alt="">
                        </div>
                    </div>
                </div>
            </section>
            <section id="reward-second">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="reward-item">
                                <div class="item-content">
                                    <h3 class="title">
                                        <span>60</span>
                                        Giải nhất
                                    </h3>
                                </div>
                                <div class="item-img">
                                    <div class="item-name">
                                        Nón bảo hiểm thể thao
                                        <br>
                                        <span class="link">monster energy</span>
                                    </div>
                                    <img src="assets/img/big_helmet.png" alt="Giải nhất">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="reward-item">
                                <div class="item-content">
                                    <h3 class="title">
                                        Hơn
                                        <span>5.000</span>
                                        giải phụ
                                    </h3>
                                </div>
                                <div class="item-img">
                                    <div class="item-name">
                                        Khăn đa năng, móc khóa
                                        <br>
                                        <span class="link">monster energy</span>
                                    </div>
                                    <img src="assets/img/Group 50.png" alt="Giải phụ">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--End reward-->
            <!--Start question-->
            <section id="faq" class="padding">
                <div class="container">
                    <div class="title-main">
                        <h2>
                            <span>hỏi đáp</span>
                        </h2>
                    </div>
                    <div class="faq">
                        <div class="list-question">
                            <div class="question"> Làm thế nào để tham gia chương trình khuyến mãi?</div>
                            <div class="answer">
                                <p>
                                    Từ
                                    <span class="link">01/10/2020 đến 31/11 /2020</span>
                                    , khi mua bất kì sản phẩm Monster Energy 355ml Lon trắng hoặc đen, tại các cửa hàng thuộc hệ thống bán lẻ Vinmart,
                                    Vinmart+, Big C, Coop Mart, Bách Hóa Xanh, Aeon Mall, Family Mart
                                    <span class="link">trên toàn quốc</span>
                                    , khách hàng sẽ được tham gia chương trình khuyến mãi 01 lần.
                                </p>
                            </div>
                        </div>
                        <div class="list-question">
                            <div class="question"> Làm thế nào để có cơ hội trúng thưởng sau khi mua hàng?</div>
                            <div class="answer">
                                <p>
                                    Đăng nhập website
                                    <span class="link">
                                        trungquamonster.com
                                    </span>
                                    để gửi thông tin và ảnh hóa đơn mua hàng. Sau đó nhận mã số quay thưởng bằng tin nhắn qua số điện thoại và email khách
                                    hàng đã cung cấp.
                                </p>
                            </div>
                        </div>
                        <div class="list-question">
                            <div class="question">
                                Làm thế nào để biết mã số quay đã trúng thưởng?
                            </div>
                            <div class="answer">
                                <p>
                                    Khách hàng trúng thưởng sẽ nhận được tin nhắn từ tổng đài 6089 với nội dung:
                                    <i>
                                        “Chuc mung ban da trung thuong [ tên giải
                                    thưởng] cua chuong trinh “TRUNG XE EXCITER 150 MONSTER ENERGY YAMAHA MOTOGP MOI TUAN”. Vui long giu hoa don mua hang,
                                    tin nhan/email thong bao Ma So Quay Thuong de lam thu tuc nhan thuong. Chi tiet: http://trungquamonster.com. Hotline
                                    1900633920.”
                                    </i>
                                </p>
                            </div>
                        </div>
                        <div class="list-question">
                            <div class="question"> Cần làm gì sau khi nhận tin nhắn thông báo trúng thưởng ?</div>
                            <div class="answer">
                                <p>
                                    Vui lòng truy cập
                                    <a href="http://trungquamonster.com" class="link">http://trungquamonster.com</a>
                                    để được hướng dẫn thủ tục nhận thưởng. Mọi chi tiết thắc mắc xin liên hệ
                                    <span class="link">
                                        Hotline 1900 633
                                    920
                                    </span>
                                    . Giải thưởng sẽ được gửi đến khách hàng qua đường bưu điện.
                                </p>
                            </div>
                        </div>
                        <div class="list-question">
                            <div class="question">Có thể tham gia chương trình nhiều lần không ?</div>
                            <div class="answer">
                                <p>
                                    Mỗi hóa đơn mua hàng trong thời gian chương trình khuyến mãi tương ứng với
                                    <span class="link">01 lần tham gia</span>
                                    . Chương trình khuyến mãi
                                    <span class="link">không giới hạn</span>
                                    số lần tham gia. Khách hàng sở hữu nhiều hơn 01 hóa đơn mua hàng Monster Energy có thể tham gia chương
                                    trình khuyến mãi nhiều lần.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--End question-->
            <!--Start footer-->
            <section id="contact" class="padding">
                <div class="container">
                    <div class="box-contact">
                        <img class="logo-ft lazy-hidden" src="assets/img/logo-ft-new.svg" alt="Logo Contact">
                        <!-- <div class="name-cp">
                        Công ty TNHH Monster Energy Việt Nam
                    </div> -->
                        <!--<div class="social-ft">
                        <a href="https://www.youtube.com/user/monsterenergy" title="Youtube" target="_blank"><img class="lazy-hidden" src="assets/img/yt-ft.png" alt="Youtube"></a>
                        <a href="https://www.facebook.com/MonsterEnergy/" title="Facebook" target="_blank"><img class="lazy-hidden" src="assets/img/fb-ft.png" alt="Facebook"></a>
                    </div> -->
                        <div class="contact-info">
                            <a href="tel:1900633920" target="_top" title="Call: 1900 633 920">
                                <div class="phone info">
                                    <div class="img phone">
                                        <img class="lazy-hidden" src="assets/img/phone.png" alt="">
                                    </div>
                                    <p>1900 633 920</p>
                                </div>
                            </a>
                            <a href="mailto:cskh@gear.monsterenergy.com" target="_top" title="Mail: cskh@gear.monsterenergy.com">
                                <div class="mail info">
                                    <div class="img mess">
                                        <img class="lazy-hidden" src="assets/img/mess.png" alt="">
                                    </div>
                                    <p>cskh@trungquamonster.com</p>
                                </div>
                            </a>
                        </div>
                        <!--  <div class="copyright">
                        Monster Energy Vietnam 2020 &copy; by MangoAds
                    </div> -->
                    </div>
                </div>
            </section>
            <!--End footer-->
            <div
                class="modal fade"
                id="video"
                tabindex="-1"
                role="dialog"
                aria-labelledby="video1Label"
                aria-hidden="true"
            >
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button
                                type="button"
                                class="close"
                                data-dismiss="modal"
                                aria-label="Close"
                            >
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="video_iframe">
                                <img alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg"></div>
            <script src="assets/js/jquery-3.3.1.min.js"></script>
            <script src="assets/js/popper.min.js"></script>
            <script src="assets/js/bootstrap.min.js"></script>
            <script src="assets/js/script.js"></script>
            
            <script type="text/javascript">
                (function(d, t) {
                    var s = d.createElement(t), options = {
                        'id': 31,
                        'container': 'c31',
                        'height': '465px',
                        'form': '//form.mangoads.com.vn/app/embed'
                    };
                    s.type= 'text/javascript';
                    s.src = 'https://form.mangoads.com.vn/static_files/js/form.widget.js';
                    s.onload = s.onreadystatechange = function() {
                        var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;
                        try { (new EasyForms()).initialize(options).display() } catch (e) { }
                    };
                    var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);
                })(document, 'script');
            </script>
            <!-- End Easy Forms -->


          
        </body>
    </html>
